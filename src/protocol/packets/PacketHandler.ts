import { varint } from '../types'
import { BufferWrapper } from '../utils/BufferWrapper'
import { Packet } from './Packet'
import { PacketBuilder } from './PacketBuilder'
import { HandshakeBuilder } from './serverBound/Handshake.packet'
import { LoginStartBuilder } from './serverBound/LoginStart.packet'
import { PingBuilder } from './serverBound/Ping.packet'
import { StatusRequestBuilder } from './serverBound/StatusRequest.packet'
import { State } from './state'

export class PacketHandler {
  static instance = new PacketHandler()

  static handshakingPackets: Map<varint, PacketBuilder<Packet>> = new Map()
    .set(0x00, HandshakeBuilder.instance)

  static statusPackets: Map<varint, PacketBuilder<Packet>> = new Map()
    .set(0x00, StatusRequestBuilder.instance)
    .set(0x01, PingBuilder.instance)

    static loginPackets: Map<varint, PacketBuilder<Packet>> = new Map()
    .set(0x00, LoginStartBuilder.instance)

  static packets: Map<State, Map<varint, PacketBuilder<Packet>>> = new Map()
    .set(State.handshaking, PacketHandler.handshakingPackets)
    .set(State.status, PacketHandler.statusPackets)
    .set(State.login, PacketHandler.loginPackets)


    
  readPacket(buff: BufferWrapper, state: State): Packet | false {
    const lenght = buff.readVarInt()
    const packetId = buff.readVarInt()

    const packetClass = PacketHandler.packets.get(state)?.get(packetId)
    if (!packetClass) {
      console.log('No packet builder for id: ' + packetId)
      return false
    }

    const packet = packetClass.fromBuffer(buff)
    buff.packetOffset += packet.totalLenght
    return packet
  }
}
